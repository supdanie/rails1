# frozen_string_literal: true

require 'test_helper'

class PeopleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @person = people(:one)
    @person2 = people(:three)
  end

  test 'should get index' do
    get people_url
    assert_response :success
  end

  test 'should get new' do
    get new_person_url
    assert_response :success
  end

  test 'should create person' do
    assert_difference('Person.count') do
      post people_url, params: { person: { email: @person.email, faculty_id: @person.faculty_id, name: @person.name, username: @person.username } }
    end

    assert_redirected_to person_url(Person.last)
  end

  test 'should show person' do
    get person_url(@person)
    assert_response :success
  end

  test 'should get edit' do
    get edit_person_url(@person)
    assert_response :success
  end

  test 'should update person' do
    patch person_url(@person), params: { person: { email: @person.email, faculty_id: @person.faculty_id, name: @person.name, username: @person.username } }
    assert_redirected_to person_url(@person)
  end

  test 'should destroy person' do
    assert_difference('Person.count', -1) do
      delete person_url(@person)
    end

    assert_redirected_to people_url
  end

  test 'should not add person with wrong email' do
    assert_difference('Person.count', 0) do
      post people_url, params: { person: { email: @person2.email, faculty_id: @person2.faculty_id, name: @person2.name, username: @person2.username } }
    end

    assert_response :success
  end

  test 'should not add person without name' do
    assert_difference('Person.count', 0) do
      post people_url, params: { person: { email: @person.email, faculty_id: @person.faculty_id, username: @person.username } }
    end

    assert_response :success
  end

  test 'should not add person without email' do
    assert_difference('Person.count', 0) do
      post people_url, params: { person: {  faculty_id: @person.faculty_id, name: @person.name, username: @person.username } }
    end

    assert_response :success
  end

  test 'should not add person without username' do
    assert_difference('Person.count', 0) do
      post people_url, params: { person: {  email: @person.email, faculty_id: @person.faculty_id, name: @person.name } }
    end

    assert_response :success
  end

  test 'should not add person without faculty' do
    assert_difference('Person.count', 0) do
      post people_url, params: { person: {  email: @person.email, name: @person.name, username: @person.username } }
    end

    assert_response :success
  end
end
