# frozen_string_literal: true

require 'test_helper'

class PublicationTest < ActiveSupport::TestCase
  test 'valid publication' do
    publication = Publication.new
    publication.title = 'Publication'
    publication.published = Date.today
    publication.abstract = 'Abstract'
    person = people(:one)
    publication.person_id = person.id
    assert publication.save, 'Not saved valid publication.'
  end

  test 'valid publication without abstract' do
    publication = Publication.new
    publication.title = 'Publication'
    publication.published = Date.today
    person = people(:one)
    publication.person_id = person.id
    assert publication.save, 'Not saved valid publication without abstract.'
  end

  test 'valid publication without abstract and published' do
    publication = Publication.new
    publication.title = 'Publication'
    person = people(:one)
    publication.person_id = person.id
    assert publication.save, 'Not saved valid publication without abstract and date of publishing.'
  end

  test 'invalid publication without author' do
    publication = Publication.new
    publication.title = 'Publication'
    publication.published = Date.today
    publication.abstract = 'Abstract'
    assert_not publication.save, 'Saved invalid publication without author.'
  end

  test 'invalid publication without name' do
    publication = Publication.new
    person = people(:one)
    publication.person_id = person.id
    publication.published = Date.today
    publication.abstract = 'Abstract'
    assert_not publication.save, 'Saved invalid publication without name.'
  end
end
