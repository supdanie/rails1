# frozen_string_literal: true

require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  test 'valid person' do
    person = Person.new
    person.name = 'Jan Novák'
    person.email = 'jannovak@seznam.cz'
    person.username = 'jannovak'
    faculty = faculties(:one)
    person.faculty_id = faculty.id
    assert person.save, 'Not saved correct person.'
  end

  test 'invalid person without faculty' do
    person = Person.new
    person.name = 'Jan Novák'
    person.email = 'jannovak@seznam.cz'
    person.username = 'jannovak'
    assert_not person.save, 'Saved person without faculty.'
  end

  test 'invalid person without name' do
    person = Person.new
    person.email = 'jannovak@seznam.cz'
    person.username = 'jannovak'
    faculty = faculties(:one)
    person.faculty_id = faculty.id
    assert_not person.save, 'Saved person without name.'
  end

  test 'invalid person without email' do
    person = Person.new
    person.name = 'Jan Novák'
    person.username = 'jannovak'
    faculty = faculties(:one)
    person.faculty_id = faculty.id
    assert_not person.save, 'Saved person without e-mail.'
  end

  test 'invalid person without username' do
    person = Person.new
    person.name = 'Jan Novák'
    person.email = 'jannovak@seznam.cz'
    faculty = faculties(:one)
    person.faculty_id = faculty.id
    assert_not person.save, 'Saved person without username.'
  end

  test 'invalid person with wrong email' do
    person = Person.new
    person.name = 'Jan Novák'
    person.email = 'jannovak'
    person.username = 'jannovak'
    faculty = faculties(:one)
    person.faculty_id = faculty.id
    assert_not person.save, 'Saved person with wrong e-mail.'
  end
end
