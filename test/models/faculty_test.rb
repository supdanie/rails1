# frozen_string_literal: true

require 'test_helper'

class FacultyTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'invalid entity without name' do
    faculty = Faculty.new
    faculty.code = 'FA'
    faculty.note = 'Note'
    assert_not faculty.save, 'Saved faculty without name.'
  end

  test 'invalid entity without code' do
    faculty = Faculty.new
    faculty.name = 'FA'
    faculty.note = 'Note'
    assert_not faculty.save, 'Saved faculty without code.'
  end

  test 'valid faculty without note' do
    faculty = Faculty.new
    faculty.name = 'Facutly'
    faculty.code = 'FA'
    assert faculty.save, 'Not saved correct faculty without note.'
  end
end
