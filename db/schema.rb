# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_191_116_163_053) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'faculties', force: :cascade do |t|
    t.string 'name'
    t.string 'code'
    t.text 'note'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'people', force: :cascade do |t|
    t.string 'name'
    t.string 'email'
    t.string 'username'
    t.bigint 'faculty_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['faculty_id'], name: 'index_people_on_faculty_id'
  end

  create_table 'publications', force: :cascade do |t|
    t.string 'title'
    t.date 'published'
    t.text 'abstract'
    t.bigint 'person_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['person_id'], name: 'index_publications_on_person_id'
  end

  add_foreign_key 'people', 'faculties'
  add_foreign_key 'publications', 'people'
end
