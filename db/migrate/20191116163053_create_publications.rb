# frozen_string_literal: true

# This class is a migration for the table with publications.
class CreatePublications < ActiveRecord::Migration[6.0]
  def change
    create_table :publications do |t|
      t.string :title
      t.date :published
      t.text :abstract
      t.references :person, null: false, foreign_key: true

      t.timestamps
    end
  end
end
