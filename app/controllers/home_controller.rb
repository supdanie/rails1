# frozen_string_literal: true

# This class is the home controller.
class HomeController < ApplicationController
  def index; end
end
