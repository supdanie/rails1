# frozen_string_literal: true

# Thic class is the base of all the controllers in the application.
class ApplicationController < ActionController::Base
end
