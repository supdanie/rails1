# frozen_string_literal: true

json.extract! publication, :id, :title, :published, :abstract, :person_id, :created_at, :updated_at
json.url publication_url(publication, format: :json)
