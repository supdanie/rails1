# frozen_string_literal: true

# This class represents a publication.
class Publication < ApplicationRecord
  belongs_to :person

  validates :title, presence: true

  def faculty
    person = Person.find(person_id)
    faculty_id = person.faculty_id
    Faculty.find(faculty_id)
  end

  def published?
    published <= Date.today
  end
end
