# frozen_string_literal: true

# This class is the parent class of all entities from database.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
