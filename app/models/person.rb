# frozen_string_literal: true

# This class represents a person.
class Person < ApplicationRecord
  belongs_to :faculty
  has_many :publications, dependent: :destroy

  validates :name, presence: true
  validates :email, presence: true
  validates :username, presence: true
  validate :email_valid

  def email_valid
    errors.add(:email, :error) unless /.+@.+(\.[^.]+)+/.match?(email)
  end

  def publications
    Publication.where(person_id: id)
  end
end
