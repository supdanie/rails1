# frozen_string_literal: true

# This class represents a faculty.
class Faculty < ApplicationRecord
  has_many :people, dependent: :destroy

  validates :name, presence: true
  validates :code, presence: true
end
